import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_unicoom_test_app/src/blocs/search/search_bloc.dart';
import 'package:flutter_unicoom_test_app/src/pages/book_page.dart';
import 'package:flutter_unicoom_test_app/src/repositories/item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unicoom_test_app/src/ui/widgets/search_bar.dart';

class SearchTab extends StatefulWidget {
  final List<Item> data;

  const SearchTab({this.data});
  @override
  _SearchTabState createState() => _SearchTabState();
}

class _SearchTabState extends State<SearchTab> {
  TextEditingController _controller;
  FocusNode _focusNode;
  String _substr = '';
  SearchBloc _searchBloc;
  //List<Item> _data = generateItems(8);

  @override
  void initState() {
    _controller = TextEditingController()..addListener(_onTextChanged);
    _focusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _controller.dispose();
    super.dispose();
  }

  void _cancelSearch() {
    _controller.clear();
    _focusNode.unfocus();
    //_animationController.reverse();
    _searchBloc.add(CloseSearchBar());
  }

  void _onTextChanged() {
    _searchBloc.add(SearchItem(items: widget.data, query: _controller.text));
    setState(() {
      _substr = _controller.text;
    });

  }

  void cancelPressed() {
    _controller.text = "";
    FocusScope.of(context).requestFocus(new FocusNode());
    _searchBloc.add(CloseSearchBar());
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  Widget _buildSearchBox() {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Expanded(
            child: SearchBar(
              controller: _controller,
              focusNode: _focusNode,
            ),
          ),
          _substr == ""
              ? Container()
              : CupertinoButton(
                  padding: EdgeInsets.only(left: 12, right: 8),
                  child: Text("Отменить",
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.red,
                          decoration: TextDecoration.underline)),
                  onPressed: () => {cancelPressed()},
                ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _searchBloc = context.bloc<SearchBloc>();

    Widget _buildPanel(List<Item> items) {
      return ExpansionPanelList.radio(
        //initialOpenPanelValue: 2,

        children: items.map<ExpansionPanelRadio>((Item item) {
          return ExpansionPanelRadio(
              canTapOnHeader: true,
              value: item.id,
              headerBuilder: (BuildContext context, bool isExpanded) {
                return ListTile(
                  title: Text(item.headerValue),
                );
              },
              body: ListTile(
                  title: Text(item.expandedValue),
                  subtitle: CupertinoButton(
                    child: Text("Подробнее"),
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.push(
                        context,
                        CupertinoPageRoute(
                          builder: (context) => BookPage(
                            bookTitle: item.headerValue,
                            bookDesc: item.expandedValue,
                          ),
                        ),
                      );
                    },
                  ),
                  onTap: () {
                    setState(() {
                      items.removeWhere((currentItem) => item == currentItem);
                    });
                  }));
        }).toList(),
      );
    }

    return Scaffold(
      backgroundColor: Colors.grey,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              _buildSearchBox(),
              BlocBuilder<SearchBloc, SearchState>(
                builder: (context, state) {
                  if (state is SearchInitial)
                    return _buildPanel(widget.data);
                  else if (state is NotSearching)
                    return _buildPanel(widget.data);
                  else if (state is SearchedItems)
                    return _buildPanel(state.items);
                  return Offstage();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
