import 'package:flutter/cupertino.dart';

const CupertinoThemeData appTheme = CupertinoThemeData(
    brightness: Brightness.light,
    primaryColor: Color(0xFFFE9526),
    scaffoldBackgroundColor: CupertinoColors.white,
    // textTheme: CupertinoTextThemeData(
    //   textStyle: TextStyle(
    //     color: Color(0xFF3F3356),
    //     fontSize: 15,
    //     fontWeight: FontWeight.w400,
    //   ),
    //   navTitleTextStyle: TextStyle(
    //     color: Color(0xFF1A051D),
    //     fontWeight: FontWeight.w600,
    //     fontSize: 17,
    //   ),
    //   actionTextStyle: TextStyle(
    //     color: CupertinoColors.white,
    //   ),
    //   tabLabelTextStyle: TextStyle(
    //     color: CupertinoColors.white,
    //     fontWeight: FontWeight.w400,
    //     fontSize: 13,
    //   ),
    // ),
  );
