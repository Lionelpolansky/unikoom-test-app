import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BookPage extends StatelessWidget {
  final String bookTitle;
  final String bookDesc;

  const BookPage({this.bookTitle, this.bookDesc});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.grey,
        centerTitle: true,
        title: Text(bookTitle),
      ),
      body: Center(
        child: Text(bookDesc,style: TextStyle(fontSize: 30),),
      ),
    );
  }
}
