import 'package:flutter/cupertino.dart';
import 'package:flutter_unicoom_test_app/src/repositories/item.dart';
import 'package:flutter_unicoom_test_app/src/repositories/item_api_client.dart';

class ItemRepository{
  final ItemApiClient itemApiClient;

  ItemRepository({@required this.itemApiClient}): assert(itemApiClient != null);

  Future<List<Item>> getProducts() async {
    //List<Item> allItems = await itemApiClient.fetchProducts();
    List<Item> allItems = generateItems(12);
    return allItems;
  }


}
