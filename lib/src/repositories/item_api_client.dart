import 'dart:convert';
import 'package:flutter_unicoom_test_app/src/repositories/item.dart';
import 'package:http/http.dart' as http;

class ItemApiClient {
  static const baseURL = 'https://api.books.com/';
  final http.Client httpClient;

  ItemApiClient(this.httpClient);

  Future<List<Item>> fetchItems() async {
    final response = await this.httpClient.get('${baseURL}catalog/');
    if (response.statusCode == 200) {
      final Json = jsonDecode(utf8.decode(response.bodyBytes)) as List;
      return Json.map((rawPost) {
        //return Item.fromJson(rawPost);
      }).toList();
    } else {
      throw Exception('error fetching posts');
    }
  }

}
