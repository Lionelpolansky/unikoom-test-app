part of 'search_bloc.dart';

@immutable
abstract class SearchEvent {}

class SearchItem extends SearchEvent {
  final String query;
  final List<Item> items;

  SearchItem({@required this.query, @required this.items});
}

class CloseSearchBar extends SearchEvent {}