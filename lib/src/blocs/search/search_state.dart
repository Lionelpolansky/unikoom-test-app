part of 'search_bloc.dart';

@immutable
abstract class SearchState {}

class SearchInitial extends SearchState {}

class SearchedItems extends SearchState {
  final List<Item> items;

  SearchedItems({@required this.items});
}

class NotSearching extends SearchState {}