import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_unicoom_test_app/src/repositories/item.dart';
import 'package:meta/meta.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc() : super(SearchInitial());

  List<Item> _filteredList = List<Item>();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is SearchItem) {
      event.items.forEach((item) {
        if (item.headerValue.toLowerCase().contains(event.query.toLowerCase())) {
          _filteredList.add(item);
        }
      });
      yield SearchedItems(items: _filteredList);
      _filteredList = [];
    } else if (event is CloseSearchBar) yield NotSearching();

  }
}
