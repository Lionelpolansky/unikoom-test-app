import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_unicoom_test_app/routing_constants.dart';
import 'package:flutter_unicoom_test_app/src/repositories/item.dart';
import 'package:flutter_unicoom_test_app/src/repositories/item_api_client.dart';
import 'package:flutter_unicoom_test_app/src/repositories/item_repository.dart';
import 'package:flutter_unicoom_test_app/src/ui/post_list_page.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart' as http;

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future loadData() async {
    List<Item> _availableItems = await ItemRepository(itemApiClient: ItemApiClient(http.Client())).getProducts();
    await Future.delayed(Duration(seconds: 2));
    //Navigator.pushReplacementNamed(context, HomePageRoute);
    Navigator.pushReplacement(context, CupertinoPageRoute(builder: (context) => SearchTab(data: _availableItems,)));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: CupertinoActivityIndicator(),
        ),
      ),
    );
  }
}
