import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_unicoom_test_app/router.dart';
import 'package:flutter_unicoom_test_app/routing_constants.dart';
import 'package:flutter_unicoom_test_app/src/blocs/search/search_bloc.dart';
import 'package:flutter_unicoom_test_app/src/utils/theme.dart';

void main() async {
  //await Hive.initFlutter();
  runApp(MyApp());
}

final router = RouterUnicoon();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarBrightness: Brightness.light,
    ));

    return MultiBlocProvider(
      providers: [
        BlocProvider<SearchBloc>(
          create: (BuildContext context) => SearchBloc(),
        ),
      ],
      child: CupertinoApp(
        debugShowCheckedModeBanner: false,
        showSemanticsDebugger: false,
        localizationsDelegates: [
          DefaultMaterialLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate,
        ],
        theme: appTheme,
        onGenerateRoute: router.generateRouteHolos,
        initialRoute: SplashScreenRoute,
      ),
    );
  }
}
