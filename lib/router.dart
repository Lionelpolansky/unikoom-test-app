import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_unicoom_test_app/routing_constants.dart';
import 'package:flutter_unicoom_test_app/src/pages/book_page.dart';
import 'package:flutter_unicoom_test_app/src/splash_screen.dart';
import 'package:flutter_unicoom_test_app/src/ui/post_list_page.dart';

class RouterUnicoon {
  Route<dynamic> generateRouteHolos(RouteSettings settings) {
    // Here we'll handle all the routing
    switch (settings.name) {
      case SplashScreenRoute:
        return CupertinoPageRoute(builder: (context) => SplashScreenPage());
      case HomePageRoute:
        return CupertinoPageRoute(builder: (context) => SearchTab());
      case BookPageRoute:
        return CupertinoPageRoute(builder: (context) => BookPage());

      default:
        return MaterialPageRoute(builder: (context) => SplashScreenPage());
    }
  }
}
